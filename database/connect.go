package database

import (
	"context"
	"github.com/ydb-platform/ydb-go-sdk/v3"
	"os"
)

func Connect(c context.Context) ydb.Connection {
	// TODO: get IAM token automatically from the YC metadata service
	db, err := ydb.Open(c, os.Getenv("DB_PASSPORT_CS"), ydb.WithAccessTokenCredentials(os.Getenv("DB_PASSPORT_IAM_TOKEN")))

	if err != nil {
		panic(err)
	}

	return db
}
