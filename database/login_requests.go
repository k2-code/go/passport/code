package database

import (
	"context"
	"errors"
	"fmt"
	"github.com/ydb-platform/ydb-go-sdk/v3"
	"github.com/ydb-platform/ydb-go-sdk/v3/table"
	"github.com/ydb-platform/ydb-go-sdk/v3/table/options"
	"github.com/ydb-platform/ydb-go-sdk/v3/table/result/named"
	"github.com/ydb-platform/ydb-go-sdk/v3/table/types"
	"passport/security"
	"passport/utils"
	"time"
)

func ProcessOTPRequest(c context.Context, db ydb.Connection, lrId string, code string) (
	err error,
) {
	lrId, lrVal, lrCode, lrAttempts, lrConfirmAttempts, lrCreatedAt, err := GetLoginRequestById(c, db, lrId)

	if err != nil {
		// TODO: create custom error with error interface
		return errors.New("cannot process your request")
	}

	// if no login request found
	if lrConfirmAttempts < 0 {
		return errors.New("cannot process your request")
	}

	// TODO: move to config
	maxAttempts := uint8(3)

	if uint8(lrConfirmAttempts) == maxAttempts {
		// TODO: create custom error with error interface
		// 15 minutes record TTL is configured on the login_requests table level
		return errors.New("too much OTP confirm requests! Wait 15 minutes and try again")
	}

	fmt.Println(code, lrCode)

	if security.ValidateArgon2Hash(code, lrCode) {
		err = DeleteLoginRequest(c, db, lrId)

		if err != nil {
			// TODO: create custom error with error interface
			return errors.New("cannot process your request")
		}

		return nil
	}

	lrConfirmAttempts += 1

	err = UpsertLoginRequest(c, db, lrId, lrCode, lrVal, lrAttempts, lrConfirmAttempts, lrCreatedAt)

	if err != nil {
		return err
	}

	return nil
}

func ProcessLoginRequest(c context.Context, db ydb.Connection, value string) (
	id string,
	code string,
	err error,
) {
	lrId, lrVal, lrAttempts, lrConfirmAttempts, lrCreatedAt, err := GetLoginRequestByValue(c, db, value)

	if err != nil {
		return "", "", err
	}

	// TODO: move to config
	maxAttempts := uint8(3)

	if uint8(lrAttempts) == maxAttempts {
		// TODO: create custom error with error interface
		// 15 minutes record TTL is configured on the login_requests table level
		return "", "", errors.New("too much login requests! Wait 15 minutes and try again")
	}

	code = utils.GenerateRandomCode()
	hash, err := security.GenerateArgon2Hash(code)

	if err != nil {
		return "", "", err
	}

	// if no login request found
	if lrAttempts < 0 {
		lrId = GenerateUuid()
		lrVal = value
		lrAttempts = 1
		lrConfirmAttempts = 0
		lrCreatedAt = uint32(time.Now().Unix())

		err = UpsertLoginRequest(c, db, lrId, hash, lrVal, lrAttempts, lrConfirmAttempts, lrCreatedAt)

		if err != nil {
			return "", "", err
		}

		return lrId, code, nil
	}

	lrAttempts += 1

	err = UpsertLoginRequest(c, db, lrId, hash, lrVal, lrAttempts, lrConfirmAttempts, lrCreatedAt)

	if err != nil {
		return "", "", err
	}

	return lrId, code, nil
}

func GetLoginRequestByValue(
	c context.Context,
	db ydb.Connection,
	value string,
) (id string, val string, attempts int32, confirmAttempts int32, createdAt uint32, err error) {
	var (
		lrId              *string
		lrVal             *string
		lrAttempts        *int32
		lrConfirmAttempts *int32
		lrCreatedAt       *uint32
	)

	// TODO: refactor. Make more flexible requests
	err = db.Table().DoTx(c, func(c context.Context, tx table.TransactionActor) (err error) {
		res, err := tx.Execute(c, `
			DECLARE $value AS Utf8;

			SELECT *
			FROM login_requests
			WHERE value = $value
			LIMIT 1
		`, table.NewQueryParameters(
			table.ValueParam("$value", types.UTF8Value(value)),
		), options.WithCollectStatsModeBasic())

		defer func() {
			_ = res.Close()
		}()

		// TODO: refactor and change on struct
		for res.NextResultSet(c) {
			for res.NextRow() {
				err = res.ScanNamed(
					named.Optional("id", &lrId),
					named.Optional("value", &lrVal),
					named.Optional("attempts", &lrAttempts),
					named.Optional("confirm_attempts", &lrConfirmAttempts),
					named.Optional("created_at", &lrCreatedAt),
				)
			}
		}

		return res.Err()
	})

	if err != nil || lrId == nil {
		return "", "", -1, -1, 0, err
	}

	return *lrId, *lrVal, *lrAttempts, *lrConfirmAttempts, *lrCreatedAt, nil
}

func GetLoginRequestById(
	c context.Context,
	db ydb.Connection,
	value string,
) (id string, val string, code string, attempts int32, confirmAttempts int32, createdAt uint32, err error) {
	var (
		lrId              *string
		lrVal             *string
		lrCode            *string
		lrAttempts        *int32
		lrConfirmAttempts *int32
		lrCreatedAt       *uint32
	)

	// TODO: refactor. Make more flexible requests
	err = db.Table().DoTx(c, func(c context.Context, tx table.TransactionActor) (err error) {
		res, err := tx.Execute(c, `
			DECLARE $value AS Utf8;

			SELECT *
			FROM login_requests
			WHERE id = $value
			LIMIT 1
		`, table.NewQueryParameters(
			table.ValueParam("$value", types.UTF8Value(value)),
		), options.WithCollectStatsModeBasic())

		defer func() {
			_ = res.Close()
		}()

		// TODO: refactor and change on struct
		for res.NextResultSet(c) {
			for res.NextRow() {
				err = res.ScanNamed(
					named.Optional("id", &lrId),
					named.Optional("value", &lrVal),
					named.Optional("code", &lrCode),
					named.Optional("attempts", &lrAttempts),
					named.Optional("confirm_attempts", &lrConfirmAttempts),
					named.Optional("created_at", &lrCreatedAt),
				)
			}
		}

		return res.Err()
	})

	if err != nil || lrId == nil {
		return "", "", "", -1, -1, 0, err
	}

	return *lrId, *lrVal, *lrCode, *lrAttempts, *lrConfirmAttempts, *lrCreatedAt, nil
}

func UpsertLoginRequest(
	c context.Context,
	db ydb.Connection,
	id string,
	codeHash string,
	value string,
	attempts int32,
	confirmAttempts int32,
	lrCreatedAt uint32,
) (err error) {
	err = db.Table().DoTx(c, func(c context.Context, tx table.TransactionActor) (err error) {
		res, err := tx.Execute(c, `
			DECLARE $id AS Utf8;
			DECLARE $code AS Utf8;
			DECLARE $value AS Utf8;

			DECLARE $attempts AS Int32;
			DECLARE $confirm_attempts AS Int32;

			DECLARE $created_at AS Datetime;
			DECLARE $updated_at AS Datetime;

			UPSERT INTO login_requests (id, code, value, attempts, confirm_attempts, created_at, updated_at)
			VALUES ($id, $code, $value, $attempts, $confirm_attempts, $created_at, $updated_at);
		`, table.NewQueryParameters(
			table.ValueParam("$id", types.UTF8Value(id)),
			table.ValueParam("$code", types.UTF8Value(codeHash)),
			table.ValueParam("$value", types.UTF8Value(value)),

			table.ValueParam("$attempts", types.Int32Value(attempts)),
			table.ValueParam("$confirm_attempts", types.Int32Value(confirmAttempts)),

			table.ValueParam("$created_at", types.DatetimeValue(lrCreatedAt)),
			table.ValueParam("$updated_at", types.DatetimeValue(uint32(time.Now().Unix()))),
		))

		if err != nil {
			return err
		}

		if err = res.Err(); err != nil {
			return err
		}

		return res.Close()
	}, table.WithIdempotent())

	if err != nil {
		return err
	}

	return nil
}

func DeleteLoginRequest(
	c context.Context,
	db ydb.Connection,
	id string,
) (err error) {
	err = db.Table().DoTx(c, func(c context.Context, tx table.TransactionActor) (err error) {
		res, err := tx.Execute(c, `
			DECLARE $id AS Utf8;
			
			DELETE FROM login_requests
			WHERE id = $id;
		`, table.NewQueryParameters(
			table.ValueParam("$id", types.UTF8Value(id)),
		))

		if err != nil {
			return err
		}

		if err = res.Err(); err != nil {
			return err
		}

		return res.Close()
	}, table.WithIdempotent())

	if err != nil {
		return err
	}

	return nil
}
