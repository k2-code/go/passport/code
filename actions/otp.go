package actions

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"passport/database"
	"passport/security"
)

type OtpRequest struct {
	Code  string `json:"code" binding:"required"`
	Token string `json:"token" binding:"required"`
}

// TODO: validations

func Otp(c *gin.Context) {
	var or OtpRequest

	if err := c.ShouldBindJSON(&or); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	db := database.Connect(c)

	// TODO: get login request here and pass it to ProcessOTPRequest

	err := database.ProcessOTPRequest(c, db, or.Token, or.Code)

	if err != nil {
		fmt.Printf("Unexpected error: %v", err)

		// TODO: write to message only public info
		// TODO: all system info should be hidden from users
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})

		return
	}

	// TODO: load user from db by login request value and if its exist, then get an id and add it to jwt
	// TODO: if it's doesn't, then create new record and add and id to jwt
	userId := "test-user-id"
	jwt, err := security.GenerateJWT(userId)

	if err != nil {
		fmt.Printf("Unexpected error: %v", err)

		// TODO: write to message only public info
		// TODO: all system info should be hidden from users
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})

		return
	}

	c.JSON(http.StatusOK, gin.H{
		"token": jwt,
	})
}
