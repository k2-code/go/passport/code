package actions

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"passport/database"
)

type LoginRequest struct {
	Value string `json:"value" binding:"required"`
}

// TODO: validations
// TODO: send message with code to Yandex Message Queue

func Login(c *gin.Context) {
	var lr LoginRequest

	if err := c.ShouldBindJSON(&lr); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	db := database.Connect(c)
	token, code, err := database.ProcessLoginRequest(c, db, lr.Value)

	if err != nil {
		fmt.Printf("Unexpected error: %v", err)

		// TODO: write to message only public info
		// TODO: all system info should be hidden from users
		c.JSON(http.StatusBadRequest, gin.H{
			"message": err.Error(),
		})

		return
	}

	// Yandex Message Queue emulator :)
	fmt.Println("Here is your confirmation code: " + code + ". Don't tell it anyone!")

	c.JSON(http.StatusOK, gin.H{
		"token": token,
	})
}
