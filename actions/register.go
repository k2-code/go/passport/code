package actions

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

type RegisterRequest struct {
	Slug      string `json:"slug" binding:"required"`
	FirstName string `json:"first_name" binding:"required"`
}

// TODO: validations
// TODO: database
// TODO: JWT
// TODO: tests

func Register(c *gin.Context) {
	var rr RegisterRequest

	if err := c.ShouldBindJSON(&rr); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	fmt.Println(rr.Slug)
	fmt.Println(rr.FirstName)

	c.JSON(http.StatusOK, gin.H{
		"token": "",
	})
}
