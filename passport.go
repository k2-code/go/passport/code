package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"passport/actions"
)

// TODO: filter proxies
// TODO: add JWT middleware
// TODO: tests
func main() {
	router := gin.Default()

	passport := router.Group("/passport")

	passport.POST("/login", actions.Login)
	passport.POST("/login/otp", actions.Otp)
	passport.POST("/register", actions.Register)

	err := router.Run()

	if err != nil {
		fmt.Println(err.Error())
	}
}
