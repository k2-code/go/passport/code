package security

import (
	"crypto/rsa"
	"github.com/golang-jwt/jwt/v4"
	"log"
	"os"
	"time"
)

// TODO: load keys from the ENV, with base64 format
const (
	privateKeyPath = "app.rsa"
	pubKeyPath     = "app.rsa.pub"
)

var (
	verifyKey *rsa.PublicKey
	signKey   *rsa.PrivateKey
)

type CustomerInfo struct {
	Id string
}

type CustomClaims struct {
	jwt.RegisteredClaims
	TokenType string
	CustomerInfo
}

func GenerateJWT(userId string) (string, error) {
	Init()

	t := jwt.New(jwt.GetSigningMethod("RS256"))

	t.Claims = &CustomClaims{
		jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(time.Minute * 1)),
		},
		"level1",
		CustomerInfo{userId},
	}

	return t.SignedString(signKey)
}

func Init() {
	signBytes, err := os.ReadFile(privateKeyPath)
	fatal(err)

	signKey, err = jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	fatal(err)

	verifyBytes, err := os.ReadFile(pubKeyPath)
	fatal(err)

	verifyKey, err = jwt.ParseRSAPublicKeyFromPEM(verifyBytes)
	fatal(err)
}

func fatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
