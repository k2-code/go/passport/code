package utils

import (
	"fmt"
	"math/rand"
	"time"
)

func GenerateRandomCode() string {
	rand.Seed(time.Now().UnixNano())

	min := 10000
	max := 99999

	return fmt.Sprint(rand.Intn(max-min) + min)
}
